package ru.goloshchapov.tm.enumerated;

public enum Status {

    NOT_STARTED ("Not Started"),
    IN_PROGRESS ("In Progress"),
    COMPLETE ("Complete");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
    
}
