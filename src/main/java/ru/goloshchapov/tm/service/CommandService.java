package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.ICommandRepository;
import ru.goloshchapov.tm.api.ICommandService;
import ru.goloshchapov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
