package ru.goloshchapov.tm.api;

public interface IProjectTaskController {

    void createTestData();

    void clear();

    void showAllByProjectId();

    void showAllByProjectName();

    void showAllByProjectIndex();

    void addTaskToProjectByIds();

    void removeTaskFromProjectByIds();

    void removeAllByProjectId();

    void removeAllByProjectName();

    void removeAllByProjectIndex();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();

}
